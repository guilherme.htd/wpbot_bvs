## Dependências
```bash
$ sudo apt install -y gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget build-essential apt-transport-https libgbm-dev
```
## Rodando a aplicação

```bash
# Ir para seu diretório home
$ cd ~

# Recuperar o script de instalação para sua versão de preferência
$ curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh

# Execute o script 
$ sudo bash nodesource_setup.sh

# Instalar o pacote Node.js
$ sudo apt install -y git nodejs yarn gcc g++ make

# Remover pacotes que não são mais necessários
$ sudo apt autoremove -y

# Clone este repositório
git clone https://gitlab.com/guilherme.htd/wpbot_bvs.git

# Acesse a pasta do projeto no terminal/cmd
$ cd wpbot

# Instale as dependências
$ npm install

# Execute a aplicação 
$ node index.js

# Manter os processos ativos a cada reinicialização do servidor
sudo npm install pm2 -g

pm2 start index.js

pm2 save

pm2 startup

sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u ${USER} --hp /home/${USER}

# O servidor iniciará na porta:8080


```
## Dockerfile
```bash
# Ir para seu diretório home
$ cd ~

# Clone este repositório
git clone https://gitlab.com/guilherme.htd/wpbot_bvs.git

# Acesse a pasta do projeto no terminal/cmd
$ cd wpbot_bvs

# Processando o arquivo Dockerfile
$ docker build -t wpbot.bvs/nodejs-wpbot:1.0 .

# Criar um contêiner
$ docker container run --name wpbot -p 8080:8080 -d wpbot.bvs/nodejs-wpbot:1.0
```

## Em desenvolvimento
Este projeto se encontra em desenvolvimento, então pode conter erros.

## License
[MIT](https://choosealicense.com/licenses/mit/)
